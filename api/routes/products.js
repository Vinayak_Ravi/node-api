const express = require("express");
const router = express.Router();

const mongoose = require("mongoose");
const Order = require("../models/order");
const Product = require("../models/product");
const service = require("../../utility/helper-service");

router.get("/", (req, res, next) => {
  Product.find()
    .select("name price _id")
    .exec()
    .then((doc) => {
      const response = {
        count: doc.length,
        products: doc.map((doc) => {
          return {
            name: doc.name,
            product_id: doc._id,
            price: doc.price,
            url: {
              type: "GET",
              url: "http://localhost:3000/products/" + doc._id,
              createdProduct: doc,
            },
          };
        }),
      };
      console.log(response);
      if (doc.length >= 0) {
        res.status(200).json(response);
      } else {
        res.status(404).json({
          messsage: "No data Found",
        });
      }
    })

    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.post("/", (req, res, next) => {
  const product = new Product(req.body);
  product
    .save()
    .then((result) => {
      console.log(result);
      res.status(201).json({
        messsage: "Product Added successfully",
        createdProduct: {
          name: result.name,
          product_id: result._id,
          price: result.price,
          request: {
            type: "GET",
            url: "http://localhost:3000/products/" + result._id,
          },
        },
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        messsage: err,
      });
    });
});

// router.post('/', (req,res,next) =>{
//     const product = {
//         name : req.body.name,
//         price : req.body.price
//     };
//     res.status(200).json({
//         message:'POST Requests',
//         createdProduct : product
//     });
// });

router.get("/:productId", (req, res, next) => {
  const id = req.params.productId;
  Product.findById(id)
    .select("name price _id")
    .exec()
    .then((doc) => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
          product: {
            name: doc.name,
            product_id: doc._id,
            price: doc.price,
            request: {
              type: "GET",
              url: "http://localhost:3000/products/" + doc._id,
            },
          },
        });
      } else {
        res.status(404).json({
          error: "No valid Product ID",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.delete("/:productId", (req, res, next) => {
  const id = req.params.productId;
  Product.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.patch("/:productId", (req, res, next) => {
  const id = req.params.productId;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  Product.update({ _id: id }, { $set: updateOps })
    .exec()
    .then((result) => {
      console.log("Checkkk" + result);
      res.status(200).json({
        product_Id: id,
        product_name: updateOps.name,
        product_price: updateOps.price,
        request: {
          type: "GET",
          url: "http://localhost:3000/products/" + id,
        },
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(401).json({
        error: err,
        messsage: "unsuceess",
      });
    });
});

router.put("/:productId", (req, res, next) => {
  const id = req.params.productId;
  console.log(req.body);
  req.body = service.objectRefracter(["available"], req.body);
  console.log(req.body);
  Product.findOneAndUpdate({ _id: id }, { $set: req.body }, { new: true })
    .then((result) => {
      console.log(Object.keys(result));
      console.log("Checkkk" + result);
      res.status(200).json({
        product: result,
        request: {
          type: "GET",
          url: "http://localhost:3000/products/" + id,
        },
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(401).json({
        error: err,
        messsage: "unsuceess",
      });
    });
});

module.exports = router;
