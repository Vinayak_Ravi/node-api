const express = require("express");

const app = express();
const morgan = require("morgan");
const bodyparser = require("body-parser");
const mongoose = require("mongoose");

mongoose
  .connect("mongodb://localhost/node-learning-api", {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then((result) => {
    console.log("monnected to mongo db");
  })
  .catch((e) => console.log(e.message));

const productRoutes = require("./api/routes/products");
const orderRoutes = require("./api/routes/orders");

app.use(morgan("dev"));
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

// app.use((req, res, next) =>{
//   res.status(200).json({
//       message:'It works!!!'
//   });
// });

app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/", (req, res) => {
  return res.json({ message: "health check" });
});
app.use((req, res, next) => {
  res.json(
    {
      error: {
        message: "Not found",
      },
    },
    404
  );
});

module.exports = app;
