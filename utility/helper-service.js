function objectRefracter(array, object) {
  for (var key in object) {
    if (array.indexOf(key) !== -1) delete object[key];
  }
  return object;
}

exports = module.exports = {
  objectRefracter,
};
